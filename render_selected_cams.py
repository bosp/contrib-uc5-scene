# render each selected camera into its own file

import bpy
import os

# export to blend file location
basedir = os.path.dirname(bpy.data.filepath)
if not basedir:
    raise Exception("Blend file is not saved")

def main(context):
    print("*** Camera selection rendering ***")
    # Keep list of user-selected objects
    selection = bpy.context.selected_objects
    bpy.ops.object.select_all(action='DESELECT')
    # Loop on selected object
    for obj in selection:
        # Consider just on selected cameras
        if (obj.type != "CAMERA"):
            continue
        # Setup the output file name based on Camare name
        name = bpy.path.clean_name(obj.name)
        name += ".avi"
        fn = os.path.join(basedir, name)
        bpy.data.scenes["Scene"].render.filepath = fn
        print("Rendering CAM: ", obj.name, ": ", fn, "...")
        # Switch to next active camera
        bpy.context.scene.camera = obj
        # Render current activated camera
        bpy.ops.render.render(animation=True)
    # Reset initial user selection
    for obj in selection:
        obj.select = True
    # Close Belnder to ensure GPU switch-off
    bpy.ops.wm.quit_blender()

class CamsRenderOperator(bpy.types.Operator):
    """Tooltip"""
    bl_idname = "object.render_selected_cams"
    bl_label = "Render Selected Cams"
    @classmethod
    def poll(cls, context):
        return context.active_object is not None
    def execute(self, context):
        main(context)
        return {'FINISHED'}
    
def register():
    bpy.utils.register_class(CamsRenderOperator)
    print("Selected Cams render: operator registered")

def unregister():
    bpy.utils.unregister_class(CamsRenderOperator)
    print("Selected Cams render: operator unregistered")
    
if __name__ == "__main__":
    register()